package com.etnetera.hr.vo;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Date;
import java.util.Set;

public class JavaScriptFrameworkVO {

	private Long id;
	private String name;
	private Date deprecationDate;
	private String hypeLevel;
	private Set<String> versions;

	@JsonCreator
	public JavaScriptFrameworkVO(String name, Date deprecationDate, String hypeLevel, Set<String> versions) {
		this.name = name;
		this.deprecationDate = deprecationDate;
		this.hypeLevel = hypeLevel;
		this.versions = versions;
	}

	public JavaScriptFrameworkVO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDeprecationDate() {
		return deprecationDate;
	}

	public String getHypeLevel() {
		return hypeLevel;
	}

	public Set<String> getVersions() {
		return versions;
	}

	public void setDeprecationDate(Date deprecationDate) {
		this.deprecationDate = deprecationDate;
	}

	public void setHypeLevel(String hypeLevel) {
		this.hypeLevel = hypeLevel;
	}

	public void setVersions(Set<String> versions) {
		this.versions = versions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "JavaScriptFrameworkVO{" +
				"name='" + name + '\'' +
				", deprecationDate=" + deprecationDate +
				", hypeLevel='" + hypeLevel + '\'' +
				", versions=" + versions +
				'}';
	}
}
