package com.etnetera.hr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.vo.JavaScriptFrameworkVO;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class JavaScriptFrameworkService {

	private final JavaScriptFrameworkRepository repository;

	@Autowired
	public JavaScriptFrameworkService(JavaScriptFrameworkRepository repository) {
		this.repository = repository;
	}

	public List<JavaScriptFrameworkVO> getAllFrameworks() {
		return StreamSupport.stream(repository.findAll().spliterator(), false)
				.map(this::toVO)
				.collect(Collectors.toList());
	}

	public JavaScriptFrameworkVO searchFramework(String name) {
		JavaScriptFramework entity = repository.findByName(name);
		if (entity == null) {
			throw new EntityNotFoundException("JavaScript framework " + name + " does not exist.");
		}
		return toVO(entity);
	}

	public JavaScriptFrameworkVO createFramework(JavaScriptFrameworkVO framework) {

		JavaScriptFramework entity = repository.findByName(framework.getName());
		if (entity != null) {
			throw new EntityExistsException("JavaScript framework " + framework.getName() + " already exists.");
		}
		JavaScriptFramework frameworkEntity = new JavaScriptFramework();
		frameworkEntity.setName(framework.getName());
		frameworkEntity.setDeprecationDate(framework.getDeprecationDate());
		frameworkEntity.setHypeLevel(framework.getHypeLevel());
		frameworkEntity.setVersions(new HashSet<>(framework.getVersions()));

		return toVO(repository.save(frameworkEntity));
	}

	public void updateFramework(long id, JavaScriptFrameworkVO framework) {

		Optional<JavaScriptFramework> entity = repository.findById(id);
		if (entity.isPresent()) {
			if (framework.getName() != null) {
				entity.get().setName(framework.getName());
			}
			if (framework.getDeprecationDate() != null) {
				entity.get().setDeprecationDate(framework.getDeprecationDate());
			}
			if (framework.getHypeLevel() != null) {
				entity.get().setHypeLevel(framework.getHypeLevel());
			}
			if (framework.getVersions() != null) {
				entity.get().setVersions(framework.getVersions());
			}
			repository.save(entity.get());
		} else {
			throw new EntityNotFoundException("JavaScript framework with id " + id + " does not exist.");
		}
	}

	public void deleteFramework(long id) {
		Optional<JavaScriptFramework> entity = repository.findById(id);
		if (entity.isPresent()) {
			repository.delete(entity.get());
		} else {
			throw new EntityNotFoundException("JavaScript framework with id " + id + " does not exist.");
		}
	}

	private JavaScriptFrameworkVO toVO(JavaScriptFramework entity) {
		JavaScriptFrameworkVO vo = new JavaScriptFrameworkVO();
		vo.setId(entity.getId());
		vo.setName(entity.getName());
		vo.setDeprecationDate(entity.getDeprecationDate());
		vo.setHypeLevel(entity.getHypeLevel());
		vo.setVersions(entity.getVersions() == null ? Collections.emptySet() : new HashSet<>(entity.getVersions()));
		return vo;
	}
}
