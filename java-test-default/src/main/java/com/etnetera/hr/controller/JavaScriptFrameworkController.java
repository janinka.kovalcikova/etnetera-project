package com.etnetera.hr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.etnetera.hr.service.JavaScriptFrameworkService;
import com.etnetera.hr.vo.JavaScriptFrameworkVO;

import javax.validation.Valid;
import java.util.List;

/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
public class JavaScriptFrameworkController {

	private final JavaScriptFrameworkService service;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptFrameworkService service) {
		this.service = service;
	}

	@GetMapping(value="/frameworks")
	public List<JavaScriptFrameworkVO> frameworks() {
		return service.getAllFrameworks();
	}

	@GetMapping(value="/frameworks/search")
	public JavaScriptFrameworkVO searchFramework(@RequestParam(value="name") String name) {
		return service.searchFramework(name);
	}

	@PostMapping(value = "/frameworks")
	public JavaScriptFrameworkVO createFramework(@RequestBody @Valid JavaScriptFrameworkVO framework) {
		return service.createFramework(framework);
	}

	@PatchMapping(value = "/frameworks/{id}")
	public void updateFramework(@PathVariable long id,
								@RequestBody @Valid JavaScriptFrameworkVO framework) {
		service.updateFramework(id, framework);
	}

	@DeleteMapping(value="/frameworks/{id}")
	public void deleteFramework(@PathVariable long id) {
		service.deleteFramework(id);
	}
}
