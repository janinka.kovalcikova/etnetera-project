package com.etnetera.hr.data;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import java.util.Date;
import java.util.Set;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity
@Table(name = "javascript_framework")
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 30)
	private String name;

	@Column(name = "deprecation_date", nullable = false)
	private Date deprecationDate;

	@Column(name = "hype_level", nullable = false, length = 30)
	private String hypeLevel;

	@ElementCollection
	@CollectionTable(name="javascript_framework_version", joinColumns = @JoinColumn(name="framework_id"))
	@Column(name = "version")
	private Set<String> versions;

	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name, Date deprecationDate, String hypeLevel, Set<String> versions) {
		this.name = name;
		this.deprecationDate = deprecationDate;
		this.hypeLevel = hypeLevel;
		this.versions = versions;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDeprecationDate() {
		return deprecationDate;
	}

	public void setDeprecationDate(Date deprecationDate) {
		this.deprecationDate = deprecationDate;
	}

	public String getHypeLevel() {
		return hypeLevel;
	}

	public void setHypeLevel(String hypeLevel) {
		this.hypeLevel = hypeLevel;
	}

	public Set<String> getVersions() {
		return versions;
	}

	public void setVersions(Set<String> versions) {
		this.versions = versions;
	}

	@Override
	public String toString() {
		return "JavaScriptFramework{" +
				"id=" + id +
				", name='" + name + '\'' +
				", deprecationDate=" + deprecationDate +
				", hypeLevel='" + hypeLevel + '\'' +
				", versions=" + versions +
				'}';
	}
}
