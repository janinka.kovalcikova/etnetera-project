package com.etnetera.hr;

import com.etnetera.hr.service.JavaScriptFrameworkService;
import org.junit.runner.RunWith;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.etnetera.hr.controller.JavaScriptFrameworkController;
import com.etnetera.hr.vo.JavaScriptFrameworkVO;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavaScriptFrameworkControllerTests {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @MockBean
    private JavaScriptFrameworkService service;

    @Test
    public void searchJavaScriptFrameworkTest() throws Exception {
        Set<String> versions = new HashSet<>(asList("v1","v8"));
        JavaScriptFrameworkVO vo = new JavaScriptFrameworkVO("Blue framework!", new Date(System.currentTimeMillis()), "level_5", versions);

        when(service.searchFramework(anyString())).thenReturn(vo);
        mvc.perform(get("/frameworks/search")
                        .param("name", "Blue framework!"))
                .andDo(print())
                .andExpect(handler().handlerType(JavaScriptFrameworkController.class))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name").value("Blue framework!"));
    }

    @Test
    public void shouldCreateFrameworkTest() throws Exception {

        SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
        Date date = DateFor.parse("08/07/2023");
        Set<String> versions = new HashSet<>(asList("v1","v3"));
        JavaScriptFrameworkVO vo = new JavaScriptFrameworkVO("Testik", date, "level1", versions);

        mvc.perform(post("/frameworks")
                        .content(mapper.writeValueAsString(vo))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(handler().handlerType(JavaScriptFrameworkController.class))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldUpdateFrameworkTest() throws Exception {

        JavaScriptFrameworkVO vo = new JavaScriptFrameworkVO();
        vo.setName("Hello new framework");
        mvc.perform(patch("/frameworks/1")
                        .content(mapper.writeValueAsString(vo))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(handler().handlerType(JavaScriptFrameworkController.class))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldDeleteFrameworkTest() throws Exception {

        mvc.perform(delete("/frameworks/1")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(handler().handlerType(JavaScriptFrameworkController.class))
                .andExpect(status().isOk());
    }

}
