package com.etnetera.hr;

import com.etnetera.hr.service.JavaScriptFrameworkService;
import com.etnetera.hr.vo.JavaScriptFrameworkVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
public class JavaScriptFrameworkServiceTests {

    @Autowired
    private JavaScriptFrameworkService service;

    @Test
    public void createJavaScriptFrameworkTest() {
        JavaScriptFrameworkVO framework = new JavaScriptFrameworkVO("Traditional framework", parseDate("15/08/2024"),
                "level_8", new HashSet<>(asList("v1","v3")));
        JavaScriptFrameworkVO created = service.createFramework(framework);

        JavaScriptFrameworkVO saved = service.searchFramework(created.getName());
        assertEquals("Traditional framework", saved.getName());
        assertEquals("level_8", saved.getHypeLevel());
        assertEquals(parseDate("15/08/2024"), saved.getDeprecationDate());
        assertEquals(2, saved.getVersions().size());
        assertTrue(saved.getVersions().containsAll(asList("v1", "v3")));
    }

    @Test(expected = EntityExistsException.class)
    public void createJavaScriptFrameworkWithExTest() {
        JavaScriptFrameworkVO framework = new JavaScriptFrameworkVO("Funny helper", parseDate("15/08/2024"),
                "level_8", new HashSet<>(asList("v1","v3")));
        service.createFramework(framework);
        service.createFramework(framework);
    }

    @Test(expected = EntityNotFoundException.class)
    public void searchJavaScriptFrameworkWithExTest() {
        service.searchFramework("Not found");
    }

    @Test
    public void updateJavaScriptFrameworkTest() {
        JavaScriptFrameworkVO framework = new JavaScriptFrameworkVO("Framework number one", parseDate("15/08/2024"),
                "level_8", new HashSet<>(asList("v1","v3")));
        JavaScriptFrameworkVO created = service.createFramework(framework);

        JavaScriptFrameworkVO toUpdate = new JavaScriptFrameworkVO();
        toUpdate.setHypeLevel("level_E");
        toUpdate.setVersions(new HashSet<>(asList("v1","v3","v6")));
        toUpdate.setDeprecationDate(parseDate("15/08/2028"));

        service.updateFramework(created.getId(), toUpdate);
        JavaScriptFrameworkVO updated = service.searchFramework(created.getName());

        assertEquals("Framework number one", updated.getName());
        assertEquals("level_E", updated.getHypeLevel());
        assertEquals(parseDate("15/08/2028"), updated.getDeprecationDate());
        assertEquals(3, updated.getVersions().size());
        assertTrue(updated.getVersions().containsAll(asList("v1", "v3", "v6")));
    }

    @Test
    public void deleteJavaScriptFrameworkTest() {
        JavaScriptFrameworkVO framework = new JavaScriptFrameworkVO("Framework number one", parseDate("15/08/2024"),
                "level_8", new HashSet<>(asList("v1","v3")));
        JavaScriptFrameworkVO framework2 = new JavaScriptFrameworkVO("Framework number two", parseDate("15/08/2024"),
                "level_8", new HashSet<>(asList("v1","v3")));
        JavaScriptFrameworkVO firstCreated = service.createFramework(framework);
        service.createFramework(framework2);

        assertEquals(2, (service.getAllFrameworks()).size());
        service.deleteFramework(firstCreated.getId());
        List<JavaScriptFrameworkVO> all = service.getAllFrameworks();
        assertEquals(1, all.size());
        assertEquals("Framework number two", all.get(0).getName());
    }

    private Date parseDate(String dateString) {
        SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return DateFor.parse(dateString);
        } catch (ParseException e) {
            throw new RuntimeException("Could not parse date");
        }
    }
}
